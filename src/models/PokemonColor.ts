export interface PokemonColors {
  name: string;
  url: string;
}

export interface GetPokemonColorsApiResponse {
  results: PokemonColors[];
  count: number;
  next: number | null;
  previous: number | null;
}
