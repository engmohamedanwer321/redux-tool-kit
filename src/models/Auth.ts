export interface LoginApiResponse {
  id: string;
  type: string;
  attributes: {
    name: string;
    email: string;
    token: string;
  };
}
