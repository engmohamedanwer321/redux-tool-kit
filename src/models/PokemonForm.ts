export interface PokemonForms {
  name: string;
  url: string;
}

export interface GetPokemonFormsApiResponse {
  results: PokemonForms[];
  count: number;
  next: number | null;
  previous: number | null;
}
