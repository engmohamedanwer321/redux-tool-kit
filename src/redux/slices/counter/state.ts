export interface StateInterface {
  value: number;
}

export const state: StateInterface = {
  value: 0,
};
