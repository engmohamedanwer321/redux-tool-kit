import {StateInterface} from './state';

const increment = (state: StateInterface): void => {
  state.value = state.value + 1;
};

const decrement = (state: StateInterface): void => {
  state.value = state.value - 1;
};

export default {
  increment,
  decrement,
};
