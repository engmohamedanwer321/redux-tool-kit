import {createSlice} from '@reduxjs/toolkit';
import {state} from './state';
import reducers from './reducers';

const counterSlice = createSlice({
  name: 'COUNTER_SLICE',
  initialState: state,
  reducers: reducers,
});

export const counterActions = counterSlice.actions;

export const counterReducer = counterSlice.reducer;
