import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import {LoginApiResponse} from '../../../models/Auth';
import {AUTH_END_POINT} from '@env';

interface Params {
  body: FormData;
}

export const authApi = createApi({
  reducerPath: AUTH_END_POINT,
  baseQuery: fetchBaseQuery({
    baseUrl: 'https://consumerstg.afeelabeta.com/api/v1',
    prepareHeaders: headers => {
      headers.set('Content-Type', 'multipart/form-data');
      return headers;
    },
  }),
  endpoints: builder => ({
    login: builder.mutation<LoginApiResponse, Params | undefined>({
      query: params => {
        console.log(params);
        return {
          url: 'login',
          method: 'POST',
          body: params?.body,
        };
      },
    }),
  }),
});

export const {useLoginMutation} = authApi;
