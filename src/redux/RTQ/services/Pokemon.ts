import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import {GetPokemonColorsApiResponse} from '../../../models/PokemonColor';
import {GetPokemonFormsApiResponse} from '../../../models/PokemonForm';
import {POKEMON_END_POINT} from '@env';

interface Params {
  token: string;
}

export const pokemonApi = createApi({
  reducerPath: 'POKEMON_API',
  baseQuery: fetchBaseQuery({
    baseUrl: POKEMON_END_POINT,
    prepareHeaders: headers => {
      headers.set('Content-Type', 'application/json');
      return headers;
    },
  }),
  endpoints: builder => ({
    getPokemonColors: builder.query<
      GetPokemonColorsApiResponse,
      Params | undefined
    >({
      query: params => {
        console.log(params);
        return {
          url: 'pokemon-color',
          method: 'GET',
          headers: {
            token: params?.token,
          },
        };
      },
    }),
    getPokemonForms: builder.query<
      GetPokemonFormsApiResponse,
      Params | undefined
    >({
      query: params => {
        console.log(params);
        return {
          url: 'pokemon-form',
          method: 'GET',
        };
      },
    }),
  }),
});

export const {useGetPokemonColorsQuery, useGetPokemonFormsQuery} = pokemonApi;
