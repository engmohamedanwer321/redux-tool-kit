import {counterActions} from './slices/counter';

const actions = {
  counter: counterActions,
};

export default {
  actions,
};
