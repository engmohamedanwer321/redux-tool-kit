import {configureStore} from '@reduxjs/toolkit';
import {counterReducer} from '../slices/counter';
import {pokemonApi} from '../RTQ/services/Pokemon';
import {authApi} from '../RTQ/services/Auth';

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    [pokemonApi.reducerPath]: pokemonApi.reducer,
    [authApi.reducerPath]: authApi.reducer,
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware().concat(pokemonApi.middleware, authApi.middleware),
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
