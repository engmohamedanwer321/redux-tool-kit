/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {FlatList, Text, TouchableOpacity, View} from 'react-native';
import {
  useGetPokemonColorsQuery,
  useGetPokemonFormsQuery,
} from '../redux/RTQ/services/Pokemon';

const PokemonColors = (): JSX.Element => {
  const [isSkipped, setIsSkipped] = useState<boolean>(true);
  const getPokemonColorsApi = useGetPokemonColorsQuery(
    {
      token: 'TOKEN',
    },
    {
      // pollingInterval: 3000,
    },
  );
  const getPokemonFormsApi = useGetPokemonFormsQuery(undefined, {
    skip: isSkipped,
  });
  console.log('isLoading => ', getPokemonColorsApi.isLoading);
  console.log('isError => ', getPokemonColorsApi.isError);
  console.log('error => ', getPokemonColorsApi.error);
  console.log('data => ', getPokemonColorsApi.data);
  console.log('data2 => ', getPokemonFormsApi.data);

  return (
    <View style={{flex: 1}}>
      {getPokemonColorsApi.isLoading ? (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>Loading...</Text>
        </View>
      ) : getPokemonColorsApi.isError ? (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>Error..., </Text>
        </View>
      ) : (
        <View>
          <TouchableOpacity
            style={{
              marginTop: 70,
              alignSelf: 'center',
              width: 100,
              height: 45,
              borderRadius: 8,
              backgroundColor: 'green',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => {
              setIsSkipped(false);
            }}>
            <Text>Refetch</Text>
          </TouchableOpacity>
          <FlatList
            data={getPokemonColorsApi.data?.results}
            renderItem={({item}) => (
              <View style={{marginVertical: 20, paddingHorizontal: 15}}>
                <Text>{item.name}</Text>
              </View>
            )}
            keyExtractor={item => item.name}
          />
        </View>
      )}
    </View>
  );
};

export default PokemonColors;
