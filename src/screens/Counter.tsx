/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {RootState} from '../redux/store';
import redux from '../redux';

const Counter = (): JSX.Element => {
  const value = useSelector<RootState, number>(state => state.counter.value);
  const dispatch = useDispatch();

  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <TouchableOpacity
          style={{
            width: 120,
            height: 45,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'black',
            borderRadius: 4,
          }}
          onPress={() => {
            dispatch(redux.actions.counter.decrement());
          }}>
          <Text style={{color: 'white', fontSize: 14}}>DECREMENT</Text>
        </TouchableOpacity>
        <Text style={{color: 'black', fontSize: 20, marginHorizontal: 20}}>
          {value}
        </Text>
        <TouchableOpacity
          style={{
            width: 120,
            height: 45,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'black',
            borderRadius: 4,
          }}
          onPress={() => {
            dispatch(redux.actions.counter.increment());
          }}>
          <Text style={{color: 'white', fontSize: 14}}>INCREMENT</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Counter;
