/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {useLoginMutation} from '../redux/RTQ/services/Auth';

const Login = (): JSX.Element => {
  const [login, {isLoading, isError, error, data, isSuccess}] =
    useLoginMutation();
  console.log('isLoading => ', isLoading);
  console.log('isError => ', isError);
  console.log('error => ', error);
  console.log('data => ', data);

  return (
    <View style={{flex: 1}}>
      <TouchableOpacity
        style={{
          marginTop: 200,
          alignSelf: 'center',
          width: 100,
          height: 45,
          borderRadius: 8,
          backgroundColor: 'green',
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={() => {
          const body = new FormData();
          body.append('email', 'musedusi@hexi.pics');
          body.append('password', '123456789');
          login({body: body});
        }}>
        <Text>Login</Text>
      </TouchableOpacity>

      {isLoading && (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>Loading...</Text>
        </View>
      )}

      {isError && (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>ERROR...</Text>
        </View>
      )}

      {isSuccess && (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>{data?.attributes.name}</Text>
          <Text>{data?.attributes.email}</Text>
        </View>
      )}
    </View>
  );
};

export default Login;
