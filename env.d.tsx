declare module '@env' {
  export const POKEMON_END_POINT: string;
  export const AUTH_END_POINT: string;
}
