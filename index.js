/**
 * @format
 */

import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import {Provider} from 'react-redux';
import {store} from './src/redux/store';
import Counter from './src/screens/Counter';
import PokemonColors from './src/screens/PokemonColors';
import Login from './src/screens/Login';

const App = () => (
  <Provider store={store}>
    {/* <Counter /> */}
    <PokemonColors />
    {/* <Login /> */}
  </Provider>
);

AppRegistry.registerComponent(appName, () => App);
